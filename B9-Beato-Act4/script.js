//Assign a variable for first name, lastname, age 
//and hobbies using the proper naming convention
let profile = {
    firstName: "Lalaine",
    lastName: "Beato",
    age: 13,
    hobbies: [
        " drawing",
        " sleeping",
        " reading",
        " eating",
        " and building someting great!"]

};

console.log(profile);
console.log(profile.firstName + " is " + profile.age + " of age.");
console.log("Her hobbies are " + profile.hobbies)

//Create a constant variable for city, house number, state and street
const address ={
    city: "Santa Rosa",
    houseNumber: "123",
    state: "Human State",
    street: "Stree B"
};

console.log(address);

//Create a function for User Information, Hobbies and Relation Status
function userInfo (hobbies, relationshipStatus){
    let ask = {hobbies:hobbies, relationshipStatus:relationshipStatus};
    return ask;
}
    let ans = userInfo("Drawing and building someting great!", "No Label",)
    console.log(ans);